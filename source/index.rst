Structure conditionnelle
====================================

Un programme peut contenir des instructions exécutées uniquement si une **condition** est vérifiée. On parle de **structure conditionnelle**. Elle se rencontre sous deux formes:

-   La structure conditionnelle sous la forme ``if ... else``
-   La structure conditionnelle sous la forme d'une boucle ``while``

Nous abordons dans cette partie ces deux structures.

.. toctree::
    :maxdepth: 1
    :hidden:
    
    content/structure_conditionnelle.rst
    content/exercice_1.rst
    content/tp_1.rst
