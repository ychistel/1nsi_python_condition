Structure conditionnelle en Python
=====================================

Condition
----------

Une **condition** est une expression qui peut prendre la valeur ``True`` si l'expression est vraie ou ``False`` si l'expression est fausse.

.. note::

    Une **condition** est aussi appelé un **test**.

Les opérateurs de comparaison permettent d'écrire des conditions simples. Les opérateurs de comparaison sont:

-  ``==`` pour comparer deux valeurs égales : ``a == 3``,
-  ``!=`` pour comparer deux valeurs non égales, différentes : ``a != 3``.
-  ``<``, ``>``, ``<=`` et ``>=`` pour comparer comme en maths : :math:`<` (est inférieur à), :math:`>` (est supérieur à), :math:`\leqslant` (est inférieur ou égal à) et :math:`\geqslant` (est supérieur ou égal à).

Prenons comme exemple, une variable ``a=5`` que l'on compare à des valeurs numériques.

.. pyscript::
    
    a=5
    print(a < 3)
    print(a!=0)
    print(a>=0)

si / if
-------

On peut soumettre l'exécution d'une instruction à une condition. La structure est de la forme:

.. code:: python

    if condition:
        instructions

La condition renvoie une valeur booléenne.

-   Si la valeur est ``True``, les instructions sont exécutées;
-   Si la valeur est ``False``, aucune instruction n'est exécutée.

Par exemple, si une variable ``a`` est positive ou nulle, alors l'affichage du message est réalisé. Dans le cas contraire, aucune instruction n'est exécutée.

.. pyscript::
    
    a=5
    if a >= 0:
        print("Le nombre a est positif")
    

si … sinon / if … else
----------------------

Lorsque la condition du ``if`` est ``False``, il est possible d'exécuter des instructions alternatives. Elles sont introduites par le mot ``else``. La structure ``if ... else`` permet de gérer les exceptions à la condition.

La structure devient :

.. code:: python

    if condition:
        instructions
    else:
        autres instructions

Par exemple, si la variable ``a`` est positive ou nulle, alors l'affichage du message "Le nombre a est positif" est réalisé. Sinon, la condition étant ``False``, le message "Le nombre a est négatif" est réalisé.

.. pyscript::

    a=5
    if a >= 0:
        print("Le nombre a est positif")
    else:
        print("Le nombre a est négatif")
    
.. warning::

    Il n'y a pas de condition placée après le ``else``. Cela correspond à tous les autres cas de figure ne vérifiant pas le test.

si … sinon si / if … elif
-------------------------

Suite à une première condition, il est possible de soumettre une nouvelle condition avec la commande ``elif``. La structure est alors de la forme:

.. code:: python

    if condition 1:
        instructions
    elif condition 2:
        autres instructions
    elif condition 3:
        encore des instructions
    else:
        les dernières instructions

.. warning::

    Le dernier ``else`` traitant les autres cas de figure n'est pas obligatoire.

Par exemple, on peut afficher un message selon le signe de celui-ci.

.. pyscript::

    a=5
    if a>0:
        print("Le nombre a est strictement positif")
    elif a<0:
        print("Le nombre a est strictement négatif")
    else:
        print("Le nombre a est nul")  
