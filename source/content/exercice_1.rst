Exercices : structure conditionnelle
====================================

.. exercice::

    On considère les variables ``a = 4`` et ``b = 7``. 

    #.  Les tests suivants sont-ils vrais ou faux ?

        a.  ``a < 5``
        b.  ``b > 7``
        c.  ``-a > -2``
        d.  ``b % a == 2`` 
        
    #.  Écrire des tests pour vérifier si :

        a.  La variable ``a`` est négative.
        b.  La variable ``a`` différente de 0.
        c.  La variable ``b`` supérieure à la variable ``a``
        d.  La variable ``b`` est divisible par la variable ``a``.

.. exercice::

    On donne le code python suivant:

    .. code:: python

        if n != 0:
            s = 1
        else:
            s = 2

    #.  Quelle est la valeur de la variable ``s`` lorsque la variable ``n`` est égale à 5 ?
    #.  Quelle est la valeur de la variable ``s`` lorsque la variable ``n`` est égale à 0 ?
    #.  Que faut-il modifier pour que la valeur de la variable ``s`` soit égale à 2 lorsque la variable ``n`` est positive ?

.. exercice::

    On donne le code python suivant:

    .. code:: python

        if m == "bonjour":
            print('vous parlez français !')
        elif m == 'hello':
            print('vous parlez anglais !')
        else:
            print('vous ne parlez plus !')

    #.  Quel est l'affichage lorsque la variable ``m`` vaut ``'bonjour'`` ?
    #.  Quelle doit être la valeur de la variable ``m`` pour avoir l'affichage ``vous ne parlez plus !`` ?
    #.  Modifier le code pour avoir aussi l'affichage ``'vous parlez espagnol !'``.
    
.. exercice::

    Pour savoir si un nombre est pair, on effectue l'opération **modulo** avec le symbole ``%``. Ce modulo donne le reste de la division entière. Par exemple : ``5 % 2`` qui vaut 1.

    #.  Quel est le reste de la division d'un nombre ``n`` par 2 lorsqu'il est pair ? impair ?
    #.  Écrire un code qui affiche si un nombre ``n`` donné est pair ou impair.

.. exercice::

    La variable ``t`` est un tableau contenant les valeurs ``t=[1,9,3,8,4,7,2,6,5]``. Pour chaque question, on reprendra le tableau ``t`` donné initialement.

    #.  Écrire un code qui remplace chaque nombre impair du tableau par 0.
    #.  Écrire un code qui affiche la valeur maximale contenue dans le tableau.
    #.  Écrire un code qui affiche la valeur minimale contenue dans le tableau.

.. exercice::

.. caution::
    
    Plutôt réservé aux élèves qui suivent la spécialité maths.

.. note::

    Il faut importer la fonction ``sqrt()`` du module ``math`` qui calcule la racine carrée d'un nombre passé en argument. Par exemple pour calculer :math:`\sqrt{9}` on écrit ``sqrt(9)`` qui renvoie la valeur 3.

    Ajouter en début de code l'instruction suivante:

    .. code::
    
        from math import sqrt

    Pour saisir une valeur entière en Python et l'affecter à une variable, on utilise les fonctions ``int`` et ``input``. On donne un exemple d'utilisation:

    .. code-block:: python

        a = int(input("valeur du coefficient: "))

#.  Écrire un code pour saisir trois nombres entiers ``a``, ``b`` et ``c`` correspondant aux coefficients d'un polynôme du second degré :math:`ax^{2}+bx+c`.
#.  Créer la variable ``discriminant`` qui qui calcule le discriminant avec les valeurs ``a``, ``b`` et ``c``. On rappelle que le discriminant est égal à :math:`b^{2}-4ac`.
#.  On donne l'algorithme du code qui affiche si le polyôme admet 0, 1 ou 2 racines selon la valeur du discriminant.

    .. code::

        si discriminant < 0:
            on affiche "le polôme n'a pas de racine."
        sinon si discriminant > 0:
            on affiche "le polynôme a 2 racines"
        sinon:
            on affiche "le polynôme a 1 racine"

#.  Compléter votre code pour afficher la valeur des racines quand elles existent. On rappelle :

    -   Le discriminant est nul alors la racine est égale à :math:`\dfrac{-b}{2a}`
    -   Le discriminant est supérieur à 0, alors les racines sont égales à :math:`\dfrac{-b-\sqrt{discriminant}}{2a}` et :math:`\dfrac{-b+\sqrt{discriminant}}{2a}`.
