La gestion du temps
=====================

Pour afficher les dates d'un mois, cela implique de connaître:

-   le premier jour du mois,
-   le mois à afficher,
-   et enfin savoir si l'année souhaitée est bissextile ou non pour le mois de février.

On définit les jours de la semaine, les mois et les dates par 3 n-uplets donnés ci-après:

.. code:: python

    jour = ("lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche")
    date = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31)
    mois = ("janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre")

On effectue pour notre programme une double saisie :

-   le mois sous la forme d'un nombre compris entre 1 et 12.
-   le premier jour du mois sous la forme d'une chaine de caractères.

Le programme à compléter est donné ci-après:

.. code:: python

    jour = ("lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche")
    date = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31)
    mois = ("janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre")

    jour = input("Quel est le premier jour du mois : ")
    mois = int(input("Quel mois doit-on afficher : "))

    # rechercher l'indice du premier jour

    if jour == "lundi":
        j = 0
    elif jour == "mardi":
        ...

    # nombre de jours du mois

    if mois in ... :
        m = 31
    elif ...:
        m = ...

    for i in range(m):
        print(f"{jour[(i+j)%7]} {date[i]} {mois}")


#.  On saisit le premier jour du mois sous la forme d'une chaine de caractères, par exemples ``"lundi"`` ou ``"mardi"``. Dans cette partie, on va écrire un code qui permet de connaître l'indice de ce jour dans le n-uplet ``jour``. Par exemple, ``lundi`` a pour indice 0 et ``samedi`` a pour indice 5. Cet indice sera mémorisé dans la variable ``j``.

    Compléter le code du programme qui recherche l'indice du premier jour du mois.

#.  On peut tester si une valeur appartient à un tableau de valeurs avec l'instruction ``in``.

    a.  Que renvoie l'instruction suivante ?

        .. code:: python

            5 in [1,4,6,8]

    b.  Que renvoie l'instruction suivante ?

        .. code:: python

            5 in [1,5,6,8]

#.  Selon le mois choisi, celui-ci a 28, 30 ou 31 jours. La variable ``m`` peut donc prendre les valeurs ``28``, ``30`` ou ``31``. On peut écrire un code qui vérifie la valeur de la variable ``mois``, mais cela représente 12 conditions à écrire ! On va utiliser des tableaux pour attribuer à la variable ``m`` le bon nombre de jours.

    a.  Quels sont les mois de 31 jours ?
    b.  Écrire un test qui vérifie si la valeur de la variable ``mois`` est un mois de 31 jours ?
    c.  Compléter le code du programme pour obtenir la bonne valeur de la variable ``m``.
